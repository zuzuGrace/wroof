class AddIndexToChowsEmail < ActiveRecord::Migration[5.2]
  def change
  	add_index :chows, :email, unique: true
  end
end
