class CreateChows < ActiveRecord::Migration[5.2]
  def change
    create_table :chows do |t|
      t.string :email
      t.string :street_address
      t.string :city
      t.string :state
      t.integer :zipcode
      t.string :pooch_name
      t.integer :age
      t.datetime :birthday
      t.text :special_needs

      t.timestamps
    end
  end
end
