class AddPasswordDigestToChows < ActiveRecord::Migration[5.2]
  def change
    add_column :chows, :password_digest, :string
  end
end
