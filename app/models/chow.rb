class Chow < ApplicationRecord
	before_save { self.email = email.downcase }
	VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
	 validates :email, presence: true, length: { maximum: 255 },

                    format: { with: VALID_EMAIL_REGEX },

                    uniqueness: { case_sensitive: false }

     validates :street_address, presence: true, length: { maximum: 255 }
     validates :city, presence: true, length: { maximum: 50 }
     validates :state, presence: true, length: { maximum: 2 }
     validates :zipcode, presence: true, numericality: {other_than: 0}
     validates :pooch_name, presence: true, length: { maximum: 50 }
     validates :age, presence: true, numericality: {other_than: 0}
     validates :birthday, presence: true 
     validates :special_needs, presence: true, length: { maximum: 350 }

    has_secure_password

    def Chow.digest(string)

    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :

                                                  BCrypt::Engine.cost

    BCrypt::Password.create(string, cost: cost)

  end
end
