class ChowsController < ApplicationController
	def index
		@chowers = Chow.all
	end

	def create
		@chow = Chow.new(chow_params)
		if @chow.save
			log_in @chow
			redirect_to @chow
		else
			#try again
		end
	end

	def show
		@chow = Chow.find(params[:id])
	end

	def edit
		@chow = Chow.find(params[:id])
	end

	def update
		@chow = Chow.find(params[:id])
		if @chow.update(chow_params)
			redirect_to @chow
		else
			#unable to update please try again
			render 'edit'
		end
	end

	def destroy
		@chow = Chow.find(params[:id])
		@chow.destroy
		redirect_to root_url
	end

	private
	def chow_params
		params.require(:chow).permit(:email, :password, :password_confirmation, :street_address, :city, :state, :zipcode, :pooch_name, :age, :birthday, :special_needs)
	end
end

