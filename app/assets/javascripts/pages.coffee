# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on "turbolinks:load", ->
		$('#baf').click ->
			$('#icons').fadeOut(100)
			$('.home-opts').fadeOut(100)
			$('#friend').fadeIn(750)

		$('#chowbttn').click ->
			$('#icons').fadeOut(100)
			$('.home-opts').fadeOut(100)
			$('#chow').fadeIn(750)


		$('#edit-chow').click ->
			$('.chow-main').fadeOut(100)
			$('#edit-acct').fadeIn(750)

		$('#days-chow').click -> 
			$('.chow-main').fadeOut(100)
			$('#chow-countdown').fadeIn(750)
