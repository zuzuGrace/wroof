Rails.application.routes.draw do
  root 'pages#home'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html


  post 'login', to: 'sessions#create'

  delete 'logout', to: 'sessions#destroy'
  resources :chows
end
